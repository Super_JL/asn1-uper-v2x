package com.hisense.hiatmp.asn.v2x;

import com.hisense.hiatmp.asn.v2x.common.Utils;
import net.gcdc.asn1.uper.UperEncoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author zhangyong
 * @date 2020/12/15  16:54
 */
public class MessageFrameTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void encode() throws Exception {

    }

    @Test
    public void decode() throws Exception {
        String bsmFrameHex = "02a4660606268666260636fe78cd1346f5d23e1c429a260606066000c5ce235f55f41fdfffe022d07d036480000000";
        String spatFrameHex = "36B6FA9DB8E6C0080008010828058082300000006CC0408C0000001B30182500000000000A08C0000001090302300000004240E094000000000048250000000118140940000000460582500000000001A08C0000000550702300000001541E09400000000000";
        String spatFrameH = "3716FC899099A53060C183060C183060C001000100B8040070424604000004D80001001013601360000400404D804D800001491810000008C00004004023002300001001008C008C00000624A04000001B80001001006E006E0000400401B801B80000249181000001360000400404D804D800010010136013600000C24604000004D80001001013601360000400404D804D800003491810000008C00004004023002300001001008C008C00000E24A04000001B80001001006E006E0000400401B801B8000040928100000050000040040140014000010010050005000000";
        byte[] mapDataFrame = {
                48, 50, 48, 48, 48, 49, 53, 57, -40, 94, 61, 9, 125, 1, 0, 0, -12, 0, 20, 3, 113, 116, -128, -51, -35, -65, 38, 91, -4, -8,
                101, -53, -110, -2, -35, 57, 32, 0, 32, 24, 72, -51, -22, -123, 85, -45, 77, -119, 65, 124, 74, -9, -40, -71, 103, 0, 1, 0,
                -63, 18, -109, -116, -128, 0, 17, 48, 35, 87, 77, 30, 34, 70, 111, 65, 20, 53, 116, -46, 75, 20, 102, -12, 86, 3, 87, 77,
                46, 29, -58, 111, 76, -76, 53, 116, -45, 86, -92, 102, -11, 46, 48, 96, 0, 32, 24, 97, 64, 90, 2, 34, 98, 0, 3, -128, 0,
                -128, 97, -64, 96, 0, 40, 70, -82, -102, 60, 68, -116, -34, -126, 40, 106, -23, -92, -106, 40, -51, -24, -84, 6, -82, -102,
                92, 59, -116, -34, -103, 104, 106, -23, -90, -83, 72, -51, -22, 92, 111, -119, 22, -5, 23, 45, 96, 0, 32, 24, 98, 82, 113,
                -112, 0, 2, 38, 4, 106, -23, -87, 113, 56, -51, -19, -21, -122, -82, -102, -117, -2, -116, -34, -52, -126, 106, -23, -89,
                -14, -120, -51, -21, -51, 6, -82, -102, 109, 121, -116, -34, -85, -26, 12, 0, 4, 3, 4, 8, 11, 64, 68, 76, 64, 0, 112, 0, 16,
                12, 24, 12, 0, 1, 8, -43, -45, 82, -30, 113, -101, -37, -41, 13, 93, 53, 23, -3, 25, -67, -103, 4, -43, -45, 79, -27, 17, -101,
                -41, -102, 13, 93, 52, -38, -13, 25, -67, 87, -52
        };
        //final MessageFrame bsmDecode = UperEncoder.decode(Utils.bytesFromHexString(bsmFrameHex), MessageFrame.class);
        final MessageFrame spatDecode = UperEncoder.decode(Utils.bytesFromHexString(spatFrameH), MessageFrame.class);
        //MessageFrame map = UperEncoder.decode(mapDataFrame, MessageFrame.class);
       //MapData mapData = UperEncoder.decode(mapDataFrame, MapData.class);
        //System.out.println(bsmDecode);
        //System.out.println(spatDecode);
        //System.out.println(mapData);
        System.out.println(Utils.hexStringFromBytes(mapDataFrame));
        System.out.println(new String(mapDataFrame));
    }
}