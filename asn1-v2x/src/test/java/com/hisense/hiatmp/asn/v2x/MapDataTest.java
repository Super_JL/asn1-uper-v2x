package com.hisense.hiatmp.asn.v2x;

import net.gcdc.asn1.uper.UperEncoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * @author zhangyong
 * @date 2020/12/15  16:53
 */
public class MapDataTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    @Test
    public void encode() throws Exception {

    }

    @Test
    public void decode() throws Exception {
        byte[] bs = new byte[]{42, 70, 96, 96, 98, 104, 102, 98,
                96, 99, 111, -25, -116, -47, 52, 111, 93, 35, -31,
                -60, 41, -94, 96, 96, 96, 102, 0, 12, 92, -30, 53,
                -11, 95, 65, -3, -1, -2, 2, 45, 7, -48, 54, 72, 0, 0, 0};
        final BasicSafetyMessage decode = UperEncoder.decode(bs, BasicSafetyMessage.class);
        System.out.println(decode);
    }
}