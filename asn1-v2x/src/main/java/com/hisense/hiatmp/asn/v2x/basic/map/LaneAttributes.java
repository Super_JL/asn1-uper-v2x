package com.hisense.hiatmp.asn.v2x.basic.map;

import lombok.Data;
import net.gcdc.asn1.datatypes.*;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributes
 * @Description
 * @CreateTime 2021/11/23
 */
@Data
@Sequence
public class LaneAttributes{
    @Component(0)
    @Asn1Optional
    LaneSharing shareWith;
    @Component(1)
    LaneTypeAttributes laneType;
}
