package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.NodeReferenceID;
import com.hisense.hiatmp.asn.v2x.basic.Phase;
import lombok.Data;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/24
 */
@Data
@Sequence
public class Connection {
    @Component(0)
    NodeReferenceID remoteIntersection;
    @Component(1)
    @Asn1Optional
    ConnectingLane connectingLane;
    @Component(2)
    @Asn1Optional
    Phase.PhaseID phaseId;

    public Connection() {
    }

    public Connection(NodeReferenceID remoteIntersection, ConnectingLane connectingLane, Phase.PhaseID phaseId) {
        this.remoteIntersection = remoteIntersection;
        this.connectingLane = connectingLane;
        this.phaseId = phaseId;
    }
}
