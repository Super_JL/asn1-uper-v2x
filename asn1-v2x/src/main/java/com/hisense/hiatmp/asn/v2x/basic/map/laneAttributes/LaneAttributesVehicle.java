package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesVehicle
 * @Description
 * @CreateTime 2021/11/24
 */
@HasExtensionMarker
@IntRange(minValue = 8, maxValue = 8)
public class LaneAttributesVehicle extends Asn1SequenceOf<Boolean> {
    public LaneAttributesVehicle() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesVehicle(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean isVehicleRevocableLane(){
        return getOrFalse(0);
    }
    public boolean isVehicleFlyOverLane(){
        return getOrFalse(1);
    }
    public boolean hovLaneUseOnly(){
        return getOrFalse(2);
    }
    public boolean restrictedToBusUse(){
        return getOrFalse(3);
    }
    public boolean restrictedToTaxiUse(){
        return getOrFalse(4);
    }
    public boolean restrictedFromPublicUse(){
        return getOrFalse(5);
    }
    public boolean hasIRbeaconCoverage(){
        return getOrFalse(6);
    }
    public boolean permissionOnRequest(){
        return getOrFalse(7);
    }
}
