package com.hisense.hiatmp.asn.v2x;

import com.hisense.hiatmp.asn.v2x.basic.DDateTime;
import com.hisense.hiatmp.asn.v2x.basic.MsgCount;
import com.hisense.hiatmp.asn.v2x.basic.map.NodeList;
import lombok.Data;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * <p>class MapData</p>
 * <p>@description </p>
 * <p>@author john</p>
 * <p>@create 2020/12/7</p>
 * <p>@version</p>
 */
@Data
@Sequence
@HasExtensionMarker
public class MapData implements AsnV2x {
    @Component(1)
    MsgCount msgCount;
    @Component(2)
    @Asn1Optional
    DDateTime.MinuteOfTheYear minuteOfTheYear;
    @Component(3)
    NodeList nodeList;

    public MapData() {
    }

    public MapData(
            MsgCount msgCount,
            DDateTime.MinuteOfTheYear minuteOfTheYear,
            NodeList nodeList) {
        this.msgCount = msgCount;
        this.minuteOfTheYear = minuteOfTheYear;
        this.nodeList = nodeList;
    }
}
