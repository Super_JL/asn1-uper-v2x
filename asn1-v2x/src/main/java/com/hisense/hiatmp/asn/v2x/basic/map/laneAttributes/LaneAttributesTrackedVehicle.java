package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesTrackedVehicle extends Asn1SequenceOf<Boolean> {
    public LaneAttributesTrackedVehicle() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesTrackedVehicle(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean spec_RevocableLane(){
        return getOrFalse(0);
    }
    public boolean spec_commuterRailRoadTrack(){
        return getOrFalse(1);
    }
    public boolean spec_lightRailRoadTrack(){
        return getOrFalse(2);
    }
    public boolean spec_heavyRailRoadTrack(){
        return getOrFalse(3);
    }
    public boolean spec_otherRailType(){
        return getOrFalse(4);
    }
}
