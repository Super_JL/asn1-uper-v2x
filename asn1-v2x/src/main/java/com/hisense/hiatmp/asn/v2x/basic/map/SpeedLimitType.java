package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.HasExtensionMarker;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName SpeedLimitType
 * @Description
 * @CreateTime 2021/11/24
 */
@HasExtensionMarker
public enum SpeedLimitType {
    unknown,
    maxSpeedInSchoolZone,
    maxSpeedInSchoolZoneWhenChildrenArePresent,
    maxSpeedInConstructionZone,
    vehicleMinSpeed,
    vehicleMaxSpeed,
    vehicleNightMaxSpeed,
    truckMinSpeed,
    truckMaxSpeed,
    truckNightMaxSpeed,
    vehiclesWithTrailersMinSpeed,
    vehiclesWithTrailersMaxSpeed,
    vehiclesWithTrailersNightMaxSpeed
}
