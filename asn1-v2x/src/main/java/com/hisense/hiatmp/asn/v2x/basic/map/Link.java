package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.DescriptiveName;
import com.hisense.hiatmp.asn.v2x.basic.NodeReferenceID;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@Data
@HasExtensionMarker
@Sequence
public class Link {
    @Component(0)
    @Asn1Optional
    DescriptiveName name;
    @Component(1)
    NodeReferenceID upstreamNodeId;
    @Component(2)
    @Asn1Optional
    SpeedLimitList speedLimits;
    @Component(3)
    @Asn1Optional
    LaneWidth linkWidth;
    @Component(4)
    @Asn1Optional
    PointList points;
    @Component(5)
    @Asn1Optional
    MovementList movements;
    @Component(6)
    LaneList lanes;

    public Link() {
    }

    public Link(
            DescriptiveName name,
            NodeReferenceID upstreamNodeId,
            SpeedLimitList speedLimits,
            LaneWidth linkWidth,
            PointList points,
            MovementList movements,
            LaneList lanes) {
        this.name = name;
        this.upstreamNodeId = upstreamNodeId;
        this.speedLimits = speedLimits;
        this.linkWidth = linkWidth;
        this.points = points;
        this.movements = movements;
        this.lanes = lanes;
    }
}
