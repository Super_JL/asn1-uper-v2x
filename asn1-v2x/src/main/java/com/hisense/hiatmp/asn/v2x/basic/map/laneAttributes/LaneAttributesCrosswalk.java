package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesCrosswalk
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesCrosswalk extends Asn1SequenceOf<Boolean> {
    public LaneAttributesCrosswalk() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesCrosswalk(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean crosswalkRevocableLane() {
        return getOrFalse(0);
    }

    public boolean bicyleUseAllowed() {
        return getOrFalse(1);
    }

    public boolean isXwalkFlyOverLane() {
        return getOrFalse(2);
    }

    public boolean fixedCycleTime() {
        return getOrFalse(3);
    }

    public boolean biDirectionalCycleTimes() {
        return getOrFalse(4);
    }

    public boolean hasPushToWalkButton() {
        return getOrFalse(5);
    }

    public boolean audioSupport() {
        return getOrFalse(6);
    }

    public boolean rfSignalRequestPresent() {
        return getOrFalse(7);
    }

    public boolean unsignalizedSegmentsPresent() {
        return getOrFalse(8);
    }
}
