package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesParking
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesParking extends Asn1SequenceOf<Boolean> {
    public LaneAttributesParking() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesParking(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean parkingRevocableLane(){
        return getOrFalse(0);
    }
    public boolean parallelParkingInUse(){
        return getOrFalse(1);
    }
    public boolean headInParkingInUse(){
        return getOrFalse(2);
    }
    public boolean doNotParkZone(){
        return getOrFalse(3);
    }
    public boolean parkingForBusUse(){
        return getOrFalse(4);
    }
    public boolean parkingForTaxiUse(){
        return getOrFalse(5);
    }
    public boolean noPublicParkingUse(){
        return getOrFalse(6);
    }
}
