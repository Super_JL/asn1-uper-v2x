package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.SizeRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/24
 */
@SizeRange(minValue = 10, maxValue = 10)
public class LaneSharing extends Asn1SequenceOf<Boolean> {
    public LaneSharing() {
        this(new ArrayList<Boolean>());
    }

    public LaneSharing(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean overlappingLaneDescriptionProvided(){
        return getOrFalse(0);
    }
    public boolean multipleLanesTreatedAsOneLane(){
        return getOrFalse(1);
    }
    public boolean otherNonMotorizedTrafficTypes(){
        return getOrFalse(2);
    }
    public boolean individualMotorizedVehicleTraffic(){
        return getOrFalse(3);
    }
    public boolean busVehicleTraffic(){
        return getOrFalse(4);
    }
    public boolean taxiVehicleTraffic(){
        return getOrFalse(5);
    }
    public boolean pedestriansTraffic(){
        return getOrFalse(6);
    }
    public boolean cyclistVehicleTraffic(){
        return getOrFalse(7);
    }
    public boolean trackedVehicleTraffic(){
        return getOrFalse(8);
    }
    public boolean pedestrianTraffic(){
        return getOrFalse(9);
    }
}
