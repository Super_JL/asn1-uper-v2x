package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.SizeRange;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@SizeRange(minValue = 1, maxValue = 32)
public class LinkList extends Asn1SequenceOf<Link> {
    public LinkList(Link ... links) {
        this(Arrays.asList(links));
    }

    public LinkList(Collection<Link> coll) {
        super(coll);
    }
}
