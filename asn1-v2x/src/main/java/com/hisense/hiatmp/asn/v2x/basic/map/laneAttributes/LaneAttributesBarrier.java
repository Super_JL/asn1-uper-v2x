package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesBarrier
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesBarrier extends Asn1SequenceOf<Boolean> {
    public LaneAttributesBarrier() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesBarrier(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }
    public boolean median_RevocableLane(){
        return getOrFalse(0);
    }
    public boolean median(){
        return getOrFalse(1);
    }
    public boolean whiteLineHashing(){
        return getOrFalse(2);
    }
    public boolean stripedLines(){
        return getOrFalse(3);
    }
    public boolean doubleStripedLines(){
        return getOrFalse(4);
    }
    public boolean trafficCones(){
        return getOrFalse(5);
    }
    public boolean constructionBarrier(){
        return getOrFalse(6);
    }
    public boolean trafficChannels(){
        return getOrFalse(7);
    }
    public boolean lowCurbs(){
        return getOrFalse(8);
    }
    public boolean highCurbs(){
        return getOrFalse(9);
    }
}
