package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1Integer;
import net.gcdc.asn1.datatypes.IntRange;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 0, maxValue = 65535)
public class NodeId extends Asn1Integer {
    public NodeId() {
        this(0);
    }

    public NodeId(int value) {
        super(value);
    }
}
