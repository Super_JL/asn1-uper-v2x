package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1Integer;
import net.gcdc.asn1.datatypes.IntRange;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 0, maxValue = 65535)
public class RoadRegulatorID extends Asn1Integer {
    public RoadRegulatorID() {
        this(0);
    }

    public RoadRegulatorID(int value) {
        super(value);
    }
}
