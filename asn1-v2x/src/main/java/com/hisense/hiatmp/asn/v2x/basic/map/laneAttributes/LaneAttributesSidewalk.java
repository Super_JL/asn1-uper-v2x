package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesSidewalk
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesSidewalk extends Asn1SequenceOf<Boolean> {
    public LaneAttributesSidewalk() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesSidewalk(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean sidewalk_RevocableLane(){
        return getOrFalse(0);
    }
    public boolean bicyleUseAllowed(){
        return getOrFalse(1);
    }
    public boolean isSidewalkFlyOverLane(){
        return getOrFalse(2);
    }
    public boolean walkBikes(){
        return getOrFalse(3);
    }
}
