package com.hisense.hiatmp.asn.v2x.basic.map;

import lombok.Data;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName Lane
 * @Description
 * @CreateTime 2021/11/23
 */
@Data
@Sequence
@HasExtensionMarker
public class Lane {
    @Component(0)
    LaneId laneId;
    @Component(1)
    @Asn1Optional
    LaneWidth laneWidth;
    @Component(2)
    @Asn1Optional
    LaneAttributes laneAttributes;
    @Component(3)
    @Asn1Optional
    AllowedManeuvers maneuvers;
    @Component(4)
    @Asn1Optional
    ConnectsToList connectsTo;
    @Component(5)
    @Asn1Optional
    SpeedLimitList speedLimits;
    @Component(6)
    @Asn1Optional
    PointList points;

    public Lane() {
    }

    public Lane(
            LaneId laneId,
            LaneWidth laneWidth,
            LaneAttributes laneAttributes,
            AllowedManeuvers maneuvers,
            ConnectsToList connectsTo,
            SpeedLimitList speedLimits,
            PointList points) {
        this.laneId = laneId;
        this.laneWidth = laneWidth;
        this.laneAttributes = laneAttributes;
        this.maneuvers = maneuvers;
        this.connectsTo = connectsTo;
        this.speedLimits = speedLimits;
        this.points = points;
    }
}
