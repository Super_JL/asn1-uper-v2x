package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1Integer;
import net.gcdc.asn1.datatypes.IntRange;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 0, maxValue = 255)
public class LaneId extends Asn1Integer {
    public LaneId() {
        this(0);
    }

    public LaneId(int value) {
        super(value);
    }
}
