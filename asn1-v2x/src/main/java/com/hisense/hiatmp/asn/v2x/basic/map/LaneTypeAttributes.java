package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes.*;
import lombok.Data;
import net.gcdc.asn1.datatypes.Choice;
import net.gcdc.asn1.datatypes.HasExtensionMarker;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneTypeAttributes
 * @Description
 * @CreateTime 2021/11/24
 */
@Data
@Choice
@HasExtensionMarker
public class LaneTypeAttributes {
    private LaneAttributesVehicle vehicle;
    private LaneAttributesCrosswalk crosswalk;
    private LaneAttributesBike bikeLane;
    private LaneAttributesSidewalk sidewalk;
    private LaneAttributesBarrier median;
    private LaneAttributesStriping striping;
    private LaneAttributesTrackedVehicle trackedVehicle;
    private LaneAttributesParking parking;

    public LaneTypeAttributes() {
    }

    public LaneTypeAttributes(
            LaneAttributesVehicle vehicle,
            LaneAttributesCrosswalk crosswalk,
            LaneAttributesBike bikeLane, LaneAttributesSidewalk sidewalk, LaneAttributesBarrier median, LaneAttributesStriping striping, LaneAttributesTrackedVehicle trackedVehicle, LaneAttributesParking parking) {
       this.vehicle = vehicle;
        this.crosswalk = crosswalk;
        this.bikeLane = bikeLane;
        this.sidewalk = sidewalk;
        this.median = median;
        this.striping = striping;
        this.trackedVehicle = trackedVehicle;
        this.parking = parking;
    }

}
