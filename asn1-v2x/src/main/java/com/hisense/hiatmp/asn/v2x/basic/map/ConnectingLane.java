package com.hisense.hiatmp.asn.v2x.basic.map;

import lombok.Data;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/24
 */
@Data
@Sequence
public class ConnectingLane {
    @Component(0)
    LaneId lane;
    @Component(1)
    @Asn1Optional
    AllowedManeuvers maneuver;

    public ConnectingLane() {
    }

    public ConnectingLane(LaneId lane, AllowedManeuvers maneuver) {
        this.lane = lane;
        this.maneuver = maneuver;
    }
}
