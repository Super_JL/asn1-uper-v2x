package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.DescriptiveName;
import com.hisense.hiatmp.asn.v2x.basic.NodeReferenceID;
import com.hisense.hiatmp.asn.v2x.basic.Position3D;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@Data
@Sequence
@HasExtensionMarker
public class Node {
    @Component(0)
    @Asn1Optional
    private DescriptiveName name;
    @Component(1)
    private NodeReferenceID id;
    @Component(2)
    private Position3D regPos;
    @Component(3)
    @Asn1Optional
    LinkList linkList;

    public Node() {
    }

    public Node(
            DescriptiveName name,
            NodeReferenceID id,
            Position3D regPos,
            LinkList linkList) {
        this.name = name;
        this.id = id;
        this.regPos = regPos;
        this.linkList = linkList;
    }
}
