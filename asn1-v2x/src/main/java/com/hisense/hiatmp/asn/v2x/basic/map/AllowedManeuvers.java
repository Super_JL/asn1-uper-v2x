package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.SizeRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@SizeRange(minValue = 12, maxValue = 12)
public class AllowedManeuvers extends Asn1SequenceOf<Boolean> {
    public AllowedManeuvers() {
        this(new ArrayList<Boolean>());
    }

    public AllowedManeuvers(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean maneuverStraightAllowed(){
        return getOrFalse(0);
    }
    public boolean maneuverLeftAllowed(){
        return getOrFalse(1);
    }
    public boolean maneuverRightAllowed(){
        return getOrFalse(2);
    }
    public boolean maneuverUTurnAllowed(){
        return getOrFalse(3);
    }
    public boolean maneuverLeftTurnOnRedAllowed(){
        return getOrFalse(4);
    }
    public boolean maneuverRightTurnOnRedAllowed(){
        return getOrFalse(5);
    }
    public boolean maneuverLaneChangeAllowed(){
        return getOrFalse(6);
    }
    public boolean maneuverNoStoppingAllowed(){
        return getOrFalse(7);
    }
    public boolean yieldAllwaysRequired(){
        return getOrFalse(8);
    }
    public boolean goWithHalt(){
        return getOrFalse(9);
    }
    public boolean caution(){
        return getOrFalse(10);
    }
    public boolean reserved1(){
        return getOrFalse(11);
    }
}
