package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.SizeRange;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName NodeList
 * @Description
 * @CreateTime 2021/11/23
 */
@SizeRange(minValue = 1, maxValue = 63)
public class NodeList extends Asn1SequenceOf<Node> {
    public NodeList(Node... nodes) {
        this(Arrays.asList(nodes));
    }

    public NodeList(Collection<Node> coll) {
        super(coll);
    }
}
