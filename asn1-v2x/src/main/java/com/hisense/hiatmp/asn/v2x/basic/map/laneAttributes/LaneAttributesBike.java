package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneAttributesBike
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesBike extends Asn1SequenceOf<Boolean> {

    public LaneAttributesBike() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesBike(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean bikeRevocableLane(){
        return getOrFalse(0);
    }
    public boolean pedestrianUseAllowed(){
        return getOrFalse(1);
    }
    public boolean isBikeFlyOverLane(){
        return getOrFalse(2);
    }
    public boolean fixedCycleTime(){
        return getOrFalse(3);
    }
    public boolean biDirectionalCycleTimes(){
        return getOrFalse(4);
    }
    public boolean isolatedByBarrier(){
        return getOrFalse(5);
    }
    public boolean unsignalizedSegmentsPresent(){
        return getOrFalse(6);
    }
}
