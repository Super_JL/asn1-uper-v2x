package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.Speed;
import lombok.Data;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName RegulatorySpeedLimit
 * @Description
 * @CreateTime 2021/11/24
 */
@Data
@Sequence
public class RegulatorySpeedLimit {
    @Component(0)
    SpeedLimitType type;
    @Component(1)
    Speed speed;

    public RegulatorySpeedLimit() {
    }

    public RegulatorySpeedLimit(SpeedLimitType type, Speed speed) {
        this.type = type;
        this.speed = speed;
    }
}
