package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.NodeReferenceID;
import com.hisense.hiatmp.asn.v2x.basic.Phase;
import lombok.Getter;
import lombok.Setter;
import net.gcdc.asn1.datatypes.Asn1Optional;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@Getter
@Setter
@Sequence
public class Movement {
    @Component(0)
    NodeReferenceID remoteIntersection;
    @Component(1)
    @Asn1Optional
    Phase.PhaseID phaseId;

    public Movement() {
    }

    public Movement(NodeReferenceID remoteIntersection, Phase.PhaseID phaseId) {
        this.remoteIntersection = remoteIntersection;
        this.phaseId = phaseId;
    }
}
