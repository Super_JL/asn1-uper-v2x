package com.hisense.hiatmp.asn.v2x.basic.map;

import com.hisense.hiatmp.asn.v2x.basic.PositionOffsetLLV;
import lombok.Data;
import net.gcdc.asn1.datatypes.Component;
import net.gcdc.asn1.datatypes.HasExtensionMarker;
import net.gcdc.asn1.datatypes.Sequence;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName RoadPoint
 * @Description
 * @CreateTime 2021/11/24
 */
@Data
@Sequence
@HasExtensionMarker
public class RoadPoint {
    @Component(0)
    PositionOffsetLLV posOffset;

    public RoadPoint() {
    }

    public RoadPoint(PositionOffsetLLV posOffset) {
        this.posOffset = posOffset;
    }
}
