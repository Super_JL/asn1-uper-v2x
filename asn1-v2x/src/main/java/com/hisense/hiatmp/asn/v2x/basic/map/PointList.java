package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 2, maxValue = 31)
public class PointList extends Asn1SequenceOf<RoadPoint> {
    public PointList(RoadPoint ... roadPoints) {
        this(Arrays.asList(roadPoints));
    }

    public PointList(Collection<RoadPoint> coll) {
        super(coll);
    }
}
