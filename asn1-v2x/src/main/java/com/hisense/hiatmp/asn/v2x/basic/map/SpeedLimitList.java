package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName SpeedLimitList
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 1, maxValue = 9)
public class SpeedLimitList extends Asn1SequenceOf<RegulatorySpeedLimit> {
    public SpeedLimitList(RegulatorySpeedLimit ... regulatorySpeedLimits) {
        this(Arrays.asList(regulatorySpeedLimits));
    }

    public SpeedLimitList(Collection<RegulatorySpeedLimit> coll) {
        super(coll);
    }
}
