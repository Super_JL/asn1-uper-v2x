package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1Integer;
import net.gcdc.asn1.datatypes.IntRange;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName LaneWidth
 * @Description
 * @CreateTime 2021/11/23
 */
@IntRange(minValue = 0, maxValue = 32767)
public class LaneWidth extends Asn1Integer {
    public LaneWidth() {
        this(0);
    }

    public LaneWidth(int value) {
        super(value);
    }
}
