package com.hisense.hiatmp.asn.v2x.basic.map;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;

import java.util.Arrays;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/23
 */
public class MovementList extends Asn1SequenceOf<Movement> {
    public MovementList(Movement ... movements) {
        this(Arrays.asList(movements));
    }

    public MovementList(Collection<Movement> coll) {
        super(coll);
    }
}
