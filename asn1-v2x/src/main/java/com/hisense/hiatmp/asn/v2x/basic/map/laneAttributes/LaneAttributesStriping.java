package com.hisense.hiatmp.asn.v2x.basic.map.laneAttributes;

import net.gcdc.asn1.datatypes.Asn1SequenceOf;
import net.gcdc.asn1.datatypes.IntRange;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @Author zhang yong
 * @Version 1.0.0
 * @ClassName
 * @Description
 * @CreateTime 2021/11/24
 */
@IntRange(minValue = 16, maxValue = 16)
public class LaneAttributesStriping extends Asn1SequenceOf<Boolean> {
    public LaneAttributesStriping() {
        this(new ArrayList<Boolean>());
    }

    public LaneAttributesStriping(Collection<Boolean> coll) {
        super(coll);
    }

    public boolean getOrFalse(int i) {
        return (i < size()) ? get(i) : false;
    }

    public boolean stripeToConnectingLanesRevocableLane(){
        return getOrFalse(0);
    }
    public boolean stripeDrawOnLeft(){
        return getOrFalse(1);
    }
    public boolean stripeDrawOnRight(){
        return getOrFalse(2);
    }
    public boolean stripeToConnectingLanesLeft(){
        return getOrFalse(3);
    }
    public boolean stripeToConnectingLanesRight(){
        return getOrFalse(4);
    }
    public boolean stripeToConnectingLanesAhead(){
        return getOrFalse(5);
    }
}
